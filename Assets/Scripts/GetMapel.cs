﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;

public class GetMapel : MonoBehaviour {

    public int jenjang;
    string urlMapel = "http://banksoalkompasilmu.com/api/pelajaran/";
    public GameObject loading;
    public int[] id;
    public string[] mapel;
    public GameObject button;
    public RectTransform vLayout;
    public Text judul;
    int[] aTime;

    void Awake()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
        if (jenjang == 5)
            judul.text = "SBMPTN";
        else if (jenjang == 6)
            judul.text = "UM UGM";
        else if (jenjang == 7)
            judul.text = "SIMAK UI";
    }

    void Start()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
        StartCoroutine(Mapel());
    }

    private IEnumerator Mapel()
    {
        string newUrl = urlMapel + PlayerPrefs.GetInt("idJenjang");
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            ParseMapel(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }

    }

    void ParseMapel(string datas)
    {
        var N = JSONNode.Parse(datas);

        id = new int[N["result"].Count];
        mapel = new string[N["result"].Count];
        if (jenjang == 5)
        {
            aTime = new int[3] { 105, 105, 105 };
            if (DataUser.Instance.mapelSbmptn.Length < N["result"].Count) DataUser.Instance.mapelSbmptn = new string[N["result"].Count];
            if (DataUser.Instance.benarSbmptn.Length < N["result"].Count) DataUser.Instance.benarSbmptn = new float[N["result"].Count];
            if (DataUser.Instance.salahSbmptn.Length < N["result"].Count) DataUser.Instance.salahSbmptn = new float[N["result"].Count];
            if (DataUser.Instance.sbmptn.Count < N["result"].Count) DataUser.Instance.newLengthAnswer(DataUser.Instance.sbmptn, N["result"].Count);
        }
        else if (jenjang == 6)
        {
            aTime = new int[4] { 30, 120, 150, 75 };
            if (DataUser.Instance.mapelUmUgm.Length < N["result"].Count) DataUser.Instance.mapelUmUgm = new string[N["result"].Count];
            if (DataUser.Instance.benarUmUgm.Length < N["result"].Count) DataUser.Instance.benarUmUgm = new float[N["result"].Count];
            if (DataUser.Instance.salahUmUgm.Length < N["result"].Count) DataUser.Instance.salahUmUgm = new float[N["result"].Count];
            if (DataUser.Instance.umUgm.Count < N["result"].Count) DataUser.Instance.newLengthAnswer(DataUser.Instance.umUgm, N["result"].Count);
        }
        else if (jenjang == 7)
        {
            aTime = new int[3] { 120, 120, 120 };
            if (DataUser.Instance.mapelSimakUi.Length < N["result"].Count) DataUser.Instance.mapelSimakUi = new string[N["result"].Count];
            if (DataUser.Instance.benarSimakUi.Length < N["result"].Count) DataUser.Instance.benarSimakUi = new float[N["result"].Count];
            if (DataUser.Instance.salahSimakUi.Length < N["result"].Count) DataUser.Instance.salahSimakUi = new float[N["result"].Count];
            if (DataUser.Instance.simakUi.Count < N["result"].Count) DataUser.Instance.newLengthAnswer(DataUser.Instance.simakUi, N["result"].Count);
        }
            

        for (int i = 0; i < N["result"].Count; i++)
        {
            mapel[i] = N["result"][i]["pelajaran"];
            id[i] = N["result"][i]["id"].AsInt;
            if (jenjang == 5)
                DataUser.Instance.mapelSbmptn[i] = N["result"][i]["pelajaran"];
            else if (jenjang == 6)
                DataUser.Instance.mapelUmUgm[i] = N["result"][i]["pelajaran"];
            else if (jenjang == 7)
                DataUser.Instance.mapelSimakUi[i] = N["result"][i]["pelajaran"];
        }

        vLayout.sizeDelta = new Vector2(vLayout.sizeDelta.x, (button.GetComponent<RectTransform>().sizeDelta.y * N["result"].Count) +
            (vLayout.GetComponent<VerticalLayoutGroup>().spacing * (N["result"].Count - 1)));

        for (int i = 0; i < N["result"].Count; i++)
        {
            if (PlayerPrefs.GetInt("CodeIsActive") == 1)
            {
                ButtonMapel(button, mapel[i], i, true, aTime[i]);
            }
            else if (PlayerPrefs.GetInt("CodeIsActive") == 2)
            {
                if (i == N["result"].Count - 1)
                    ButtonMapel(button, mapel[i], i, false, aTime[i]);
                else
                    ButtonMapel(button, mapel[i], i, true, aTime[i]);
            }
            else if(PlayerPrefs.GetInt("CodeIsActive") == 3)
            {
                if (i == N["result"].Count - 2)
                    ButtonMapel(button, mapel[i], i, false, aTime[i]);
                else
                    ButtonMapel(button, mapel[i], i, true, aTime[i]);
            }
        }

        loading.SetActive(false);
    }

    public void ButtonMapel(GameObject button, string mapel, int id, bool isEnable, int time)
    {
        var newButton = Instantiate(button, vLayout.localPosition, Quaternion.identity) as GameObject;
        newButton.transform.parent = vLayout.transform;
        newButton.transform.localScale = new Vector3(1, 1, 1);
        newButton.GetComponentInChildren<Text>().text = mapel;
        newButton.GetComponent<IdMapel>().mapel = mapel;
        newButton.GetComponent<IdMapel>().time = time;
        newButton.GetComponent<IdMapel>().id = id;
        newButton.GetComponent<Button>().interactable = isEnable;
        if (!isEnable)
            newButton.transform.GetChild(0).GetComponent<Text>().color = new Color(1, 1, 1, .5f);
    }
}
