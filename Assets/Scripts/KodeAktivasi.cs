﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KodeAktivasi : MonoBehaviour {

    public string[] kode;

    public InputField inputText;

    void Awake()
    {
        if (PlayerPrefs.GetInt("CodeIsActive") > 0)
            Application.LoadLevel("Home");
    }

    public void nextBtn()
    {
        for (int i = 0; i < kode.Length; i++)
        {
            if (inputText.text == kode[i])
            {
                Application.LoadLevel("Home");
                PlayerPrefs.SetInt("CodeIsActive", i+1);
                return;
            }
            if(inputText.text != kode[i] && i == kode.Length - 1)
            {
                for (int o = 1; o < inputText.transform.childCount - 1; o++)
                {
                    inputText.transform.GetChild(o).GetComponent<Text>().color = Color.red;
                }
                StartCoroutine("backToWhite");
            }
        }
    }

    IEnumerator backToWhite()
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = 1; i < inputText.transform.childCount - 1; i++)
        {
            inputText.transform.GetChild(i).GetComponent<Text>().color = Color.white;
        }
    }
}
