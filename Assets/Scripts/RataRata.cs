﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class RataRata : MonoBehaviour {
    
    DataUser user;

    public GameObject mapel;
    public GameObject hasil;

    public Text skorSains;
    public Text pgSains;

    public Text skorSoshum;
    public Text pgSoshum;

    public Color newColor;

    public GameObject back;
    public GameObject backHome;

    void Start ()
    {
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
    }

    public void ShowHasil(int idJenjang)
    {
        mapel.SetActive(false);
        hasil.SetActive(true);
        back.SetActive(true);
        backHome.SetActive(false);
        Hasil(idJenjang);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().backgroundColor = Color.white;
    }

    public void Back()
    {
        mapel.SetActive(true);
        hasil.SetActive(false);
        back.SetActive(false);
        backHome.SetActive(true);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().backgroundColor = newColor;
    }

    void Hasil(int idJenjang)
    {
        var sains = 0;
        var soshum = 0;
        var skorPGSains = 0.0f;
        var skorPGSoshum = 0.0f;

        if (idJenjang == 5)
        {
            for(int i = 0; i < user.benarSbmptn.Length; i++)
            {
                if (i != user.benarSbmptn.Length - 1)
                    sains += (int)((user.benarSbmptn[i] * 4) - user.salahSbmptn[i]);

                if (i != user.benarSbmptn.Length - 2)
                    soshum += (int)((user.benarSbmptn[i] * 4) - user.salahSbmptn[i]);
            }
            skorPGSains = (float) Math.Round(((float)sains / 600) * 100, 2);
            skorPGSoshum = (float) Math.Round(((float)soshum / 600) * 100, 2);
        }
        else if (idJenjang == 6)
        {
            for (int i = 0; i < user.benarUmUgm.Length; i++)
            {
                if (i != user.benarUmUgm.Length - 1)
                    sains += (int)((user.benarUmUgm[i] * 4) - user.salahUmUgm[i]);

                if (i != user.benarUmUgm.Length - 2)
                    soshum += (int)((user.benarUmUgm[i] * 4) - user.salahUmUgm[i]);
            }
            skorPGSains = (float)Math.Round(((float)sains / 780) * 100, 2);
            skorPGSoshum = (float)Math.Round(((float)soshum / 780) * 100, 2);
        }
        else if (idJenjang == 7)
        {
            for (int i = 0; i < user.benarSimakUi.Length; i++)
            {
                if (i != user.benarSimakUi.Length - 1)
                    sains += (int)((user.benarSimakUi[i] * 4) - user.salahSimakUi[i]);

                if (i != user.benarSimakUi.Length - 2)
                    soshum += (int)((user.benarSimakUi[i] * 4) - user.salahSimakUi[i]);
            }
            skorPGSains = (float)Math.Round(((float)sains / 480) * 100, 2);
            skorPGSoshum = (float)Math.Round(((float)soshum / 480) * 100, 2);
        }

        skorSains.text = "Total Skor\t\t\t\t\t\t\t\t\t\t: " + sains;
        pgSains.text = "PG\t\t\t\t\t\t\t\t\t\t\t\t: " + skorPGSains + "%";
        skorSoshum.text = "Total Skor\t\t\t\t\t\t\t\t\t\t: " + soshum;
        pgSoshum.text = "PG\t\t\t\t\t\t\t\t\t\t\t\t: " + skorPGSoshum + "%";
    }
}
