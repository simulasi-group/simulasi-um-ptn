﻿using UnityEngine;
using System.Collections;

public class IdMapel : MonoBehaviour {

    public int id;
    public string mapel;
    public int time;

	public void StartUN()
    {
        GameObject.Find("Mapel").GetComponent<ButtonScript>().indexSoal = id;
        GameObject.Find("Mapel").GetComponent<ButtonScript>().time = time;
        GameObject.Find("Mapel").GetComponent<ButtonScript>().petunjuk.SetActive(true);
    }

    public void Review()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Review>().ShowReviewData(mapel);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Review>().SetMapel(id);
    }
}
