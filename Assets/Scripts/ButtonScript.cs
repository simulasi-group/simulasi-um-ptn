﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using SimpleJSON;

public class ButtonScript : MonoBehaviour {

    public GameObject soal;
    public GameObject petunjuk;
    public int indexSoal;
	public int time;

	public string pembahasanUrl = "http://banksoalkompasilmu.com/api/download/";

	public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void SubMapel(int idJenjang)
    {
        PlayerPrefs.SetInt("idJenjang", idJenjang);
    }

    public void StartUN(int indexSoal, int time)
    {
        soal.SetActive(true);
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().StartUN(GetComponent<GetMapel>().id[indexSoal], time);
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().mapel = indexSoal;
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().fieldMapel[0].text = GetComponent<GetMapel>().mapel[indexSoal];
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().fieldMapel[1].text = GetComponent<GetMapel>().mapel[indexSoal];
        Destroy(petunjuk);
        Destroy(gameObject);
    }

    public void ReadyUM()
    {
        StartUN(indexSoal, time);
    }

    public void BackPetunjuk()
    {
        petunjuk.SetActive(false);
	}

	public void detailPembahasan(int id) {
		PlayerPrefs.SetInt ("DetailPembahasan", id);
		SceneManager.LoadScene("DetailPembahasan");
	}

	public void downloadPembahasan(int id) {
		string savePath = Application.persistentDataPath;
		string nameMapel = "";
		if (id == 15)
			nameMapel = "SAINTEK";
		else if (id == 16)
			nameMapel = "SOSHUM";
		if (File.Exists (savePath + "/Pembahasan_" + nameMapel + ".pdf") && !isInternetConnection()) {
			Application.OpenURL(savePath+"/Pembahasan_" + nameMapel + ".pdf");
			return;
		}
		StartCoroutine (getLink (id, nameMapel));
	}

	IEnumerator getLink(int id, string nameMapel) {
		WWW www = new WWW(pembahasanUrl + id);
		yield return www;
		if (www.error == null)
		{
			StartCoroutine (Parse (www.text, nameMapel));
		}
		else
		{
			Application.LoadLevel("Home");
			Debug.Log(www.error);
		}
	}

	IEnumerator Parse(string data, string nameMapel) {
		var N = JSONNode.Parse(data);
		var path = N ["link"];

		string savePath = Application.persistentDataPath;
		WWW www = new WWW(path);
		yield return www;

		byte[] bytes = www.bytes;
		try{
			File.WriteAllBytes(savePath+"/Pembahasan_" + nameMapel + ".pdf", bytes);
		}catch(Exception ex){
			Debug.Log (ex.Message);
		}
		Application.OpenURL(path);
	}

	bool isInternetConnection()
	{
		bool isConnectedToInternet = false;
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
			Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			isConnectedToInternet = true;
		}
		return isConnectedToInternet;
	}
}
