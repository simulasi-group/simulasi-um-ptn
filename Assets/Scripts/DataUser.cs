﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataUser : MonoBehaviour {

    private static DataUser instance = null;

    public List<MyAnswer> sbmptn = new List<MyAnswer>();
    public List<MyAnswer> umUgm = new List<MyAnswer>();
    public List<MyAnswer> simakUi = new List<MyAnswer>();
    public float[] benarSbmptn;
    public float[] benarUmUgm;
    public float[] benarSimakUi;
    public float[] salahSbmptn;
    public float[] salahUmUgm;
    public float[] salahSimakUi;
    public string[] mapelSbmptn;
    public string[] mapelUmUgm;
    public string[] mapelSimakUi;

    public static DataUser Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void newLengthAnswer(List<MyAnswer> newAnswer, int count)
    {
        for (int i = 0; i < count; i++)
        {
            var arr = new MyAnswer();
            newAnswer.Add(arr);
        }
    }
}